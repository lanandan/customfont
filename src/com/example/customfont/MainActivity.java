package com.example.customfont;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends Activity {

	
	TextView txt1,txt2,txt3,txt4,txt5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "milit.ttf");
        Typeface tf1 = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "molten.ttf");
        Typeface tf2 = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "mypager.ttf");
        Typeface tf3 = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "Demo_ConeriaScript.ttf");
        Typeface tf4 = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "diager.ttf");
        
        txt1=(TextView)findViewById(R.id.textView1);
        txt2=(TextView)findViewById(R.id.textView2);
        txt3=(TextView)findViewById(R.id.textView3);
        txt4=(TextView)findViewById(R.id.textView4);
        txt5=(TextView)findViewById(R.id.textView5);
        
        txt1.setTypeface(tf);
        txt2.setTypeface(tf1);
        txt3.setTypeface(tf2);
        txt4.setTypeface(tf3);
        txt5.setTypeface(tf4);
        
    }



}
